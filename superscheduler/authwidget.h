#ifndef AUTHWIDGET_H
#define AUTHWIDGET_H

#include <QWidget>
#include <QPushButton>

namespace Ui {
class AuthWidget;
}

class AuthWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AuthWidget(QWidget *parent = nullptr);
    ~AuthWidget();

//    Ui::AuthWidget* getUi() {
//        return ui;
//    }

    QPushButton* getAuthButton();
    QPushButton* getRegButton();

private:
    Ui::AuthWidget *ui;
};

#endif // AUTHWIDGET_H
