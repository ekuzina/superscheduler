#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_authWidget = new AuthWidget(ui->widget);
    m_scheduleWidget = new ScheduleWidget(ui->widget);
    m_scheduleWidget->setHidden(true);
    m_authWidget->getAuthButton();

    connect(m_authWidget->getAuthButton(), SIGNAL(clicked()), this, SLOT(onAuthButtonClicked()));
    connect(m_authWidget->getRegButton(), SIGNAL(clicked()), this, SLOT(onRegButtonClicked()));
    connect(m_scheduleWidget->getLogOutButton(), SIGNAL(clicked()), this, SLOT(onLogOutButtonClicked()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onAuthButtonClicked()
{
    m_authWidget->setHidden(true);
    m_scheduleWidget->setHidden(false);
}

void MainWindow::onRegButtonClicked()
{
    onAuthButtonClicked();
}

void MainWindow::onLogOutButtonClicked()
{
    m_authWidget->setHidden(false);
    m_scheduleWidget->setHidden(true);
}

